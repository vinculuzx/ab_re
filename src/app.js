var modal = document.getElementsByClassName("modal");

var addClick = () => {
    let i;
    var elementLI = document.getElementsByClassName("tab");
    for (i = 0; i < elementLI.length; i++) {
        elementLI[i].addEventListener("click", (data) => {
            showContent(data.toElement.textContent)
        });
    }
}
addClick()

// show on screen tab content

var showContent = (item) => {
    let i;
    var contentTab = document.getElementsByClassName("content");
    for (i = 0; i < contentTab.length; i++) {
        contentTab[i].style.display = "none";
    }
    document.getElementById(item).style.display = "block";
}

// open modal popup

var openModal = () => {
    let i;
    let aboutEdit = document.getElementsByClassName("ion-edit");
    for (i = 0; i < aboutEdit.length; i++) {
        aboutEdit[i].addEventListener("click", () => {
            modal[0].style.display = "block";
        })
    }
}
openModal()