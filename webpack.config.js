var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: [
        'babel-polyfill',
        './src/app.js'
    ],
    output: {
        publicPath: '/',
        filename: 'build.js'
    },
    devtool: 'source-map',
    devServer: {
        contentBase: "./"
    },
    module: {
        rules: [{
            test: /\.js$/,
            include: path.join(__dirname, 'src'),
            loader: 'babel-loader',
            query: {
                presets: ["es2015"],
            }
        }]
    },
};