var gulp = require('gulp');
var gulpsass = require('gulp-sass');
var gulprename = require('gulp-rename');

// SASS folder
var sassFile = './src/sass/global.scss';

// css Destination
var cssDest = './css'

// Options for development
var sassDevOptions = {
    outputStyle: 'expanded'
}

// Options for production
var sassProdOptions = {
    outputStyle: 'compressed'
}

// sass compiler development enviroment
gulp.task('sassDev', function() {
    return gulp.src(sassFile)
        .pipe(gulpsass(sassDevOptions).on('error', gulpsass.logError))
        .pipe(gulp.dest(cssDest));
});

// sass compiler production enviroment
gulp.task('sassPrd', function() {
    return gulp.src(sassFile)
        .pipe(gulpsass(sassProdOptions).on('error', gulpsass.logError))
        .pipe(gulprename('global.min.css'))
        .pipe(gulp.dest(cssDest));
});

// taks gulp sass
gulp.task('watch', function() {
    gulp.watch(sassFile, ['sassDev', 'sassPrd']);
});
// default task
gulp.task('default', ['sassDev', 'sassPrd', 'watch'])